//custom js file 
jQuery(document).ready(function($) {

	//validate javascript.
	$('#frmAddAvailableDates').submit(function(e){
		e.preventDefault();
		
		var fistName = $('#txtFirstName').val();
		var email = $('#txtEmail').val();

		if ( fistName == "" ) {
			$('span.txtFirstName').html('Fist Name fields is required' ).css("color", "red" );
		}else if ( email == "" ) {
			$('span.txtEmail').html('Email field is required' ).css("color", "red" );
		}else {
			$.ajax({
				url:CustomPlugin.admin_ajax,
				type:'POST',
				data:{
					action: 'saveFixedDepartureDates',
					data: $(this).serialize(),
				},
				beforeSend: function( xhr ) {
					$('#ImgLoder').show();
				},
				success:function(results){
					if ( true === results.success ) {
						$('#ImgLoder').hide();
						$("#MessageDone").html(results.data).show();
						jQuery('#frmAddAvailableDates')[0].reset();
					}

					if ( false === results.success ) {
						$('#ImgLoder').hide();
						$("#MessageFail").html(results.data).show();
					}
				}
			});
		}

	});

	//Edit available dates 
	$('#frmEditAvailableDates').submit(function(e){
		e.preventDefault();
		
		var fistName = $('#txtFirstName').val();
		var email = $('#txtEmail').val();

		if ( fistName == "" ) {
			$('span.txtFirstName').html('Fist Name fields is required' ).css("color", "red" );
		}else if ( email == "" ) {
			$('span.txtEmail').html('Email field is required' ).css("color", "red" );
		} else {
			$.ajax({
				url:CustomPlugin.admin_ajax,
				type:'POST',
				data:{
					action: 'editFixedDepartureDates',
					data: $(this).serialize(),
				},
				beforeSend: function( xhr ) {
					$('#ImgLoder').show();
				},
				success:function(results){
					if ( true === results.success ) {
						$('#ImgLoder').hide();
						$("#MessageDone").html(results.data).show();
					}

					if ( false === results.success ) {
						$('#ImgLoder').hide();
						$("#MessageFail").html(results.data).show();
					}
				}
			});
		}

	});

	//addCategory Form
	$('#frmAddCategory').submit(function(e){
		e.preventDefault();
		
		var categoryName = $('#txtCategoryName').val();

		if ( categoryName == "" ) {
			$('span.txtCategoryName').html('Category name is required' ).css("color", "red" );
		}else {
			$.ajax({
				url:CustomPlugin.admin_ajax,
				type:'POST',
				data:{
					action: 'saveMemberCategory',
					data: $(this).serialize(),
				},
				beforeSend: function( xhr ) {
					$('#ImgLoder').show();
				},
				success:function(results){
					if ( true === results.success ) {
						$('#ImgLoder').hide();
						$("#MessageDone").html(results.data).show();
						jQuery('#frmAddCategory')[0].reset();
					}

					if ( false === results.success ) {
						$('#ImgLoder').hide();
						$("#MessageFail").html(results.data).show();
					}
				}
			});
		}

	});

});