<?php
/*
Plugin Name:  DWT Email Light
Plugin URI:   https://dipakdhakal.com/
Description:  This is Simple wordpress Plugin build for email management.
Version:      1.0.0
Author:       Dipak Dhakal
Author URI:   https://dipakdhakal.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  dwt_email_light
*/

use \Inc\Base\MyDb;

if( ! defined( 'ABSPATH' ) ){
	die;
}

if( file_exists( dirname( __FILE__ ) .'/vendor/autoload.php') ){
	require_once dirname( __FILE__ ) .'/vendor/autoload.php';
}

/**
* @package Dwt Travel
* @return
* @method activation method
*/

function fixed_departure_activate(){
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'fixed_departure_activate');

/**
* @package Dwt Travel
* @return
* @method deactivation method
*/

function fixed_departure_deactivate(){
	Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'fixed_departure_deactivate');


/**
* @package Dwt Travel
* @return
* @method call init base class
*/
if( class_exists( 'Inc\\Init') ){
	Inc\Init::register_services();
}

/**
* @package Dwt Travel
* @return
* @method database activation method
*/

function fixed_departure_createdatabase(){
	$my_db = new MyDb();
    $capsule = $my_db->capsule();
	new Inc\Base\CreateDatabaseTable( $capsule );
}
register_activation_hook( __FILE__, 'fixed_departure_createdatabase');

