<?php
namespace Inc\templates;

use \Inc\Base\CrudMethod;
use \Inc\Base\AjaxCallBack;

/**
* @package Dwt Travel
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

if ( is_user_logged_in() ) {
	if ( current_user_can( 'manage_options' ) ) { 
        $memberCategories = CrudMethod::getMemberCategoryLists();
        ?>
		<div class="wrap custom-plugin">
		<h1>Add Member </h1>
		
		<div class="row">
			<div class="col-lg-6 test">
                <div class="contact-form-wrap white-bg">
                    <form id="frmAddAvailableDates" method="post">

                        <div class="form-group">
                            <label for="bookingdate">First Name</label>
                            <input type="text" name="txtFirstName" class="form-control" id="txtFirstName">
                            <span class="txtFirstName"></span>
                        </div>
                        
                        <div class="form-group">
                            <label for="mobile">Last Name</label>
                            <input type="text" class="form-control" name="txtLastName" id="txtLastName">
                            <span class="txtLastName"></span>
                        </div>

                        <div class="form-group">
                            <label for="message">Email</label>
                            <input type="text" class="form-control" name="txtEmail" id="txtEmail">
                            <span class="txtEmail"></span>
                        </div>
                        <div class="form-group">
                            <label for="message">District</label>
                            <input type="text" class="form-control" name="txtDistrict" id="txtDistrict">
                            <span class="txtDistrict"></span>
                        </div>
                        <div class="form-group">
                            <label for="message">Designation</label>
                            <input type="text" class="form-control" name="txtDesignation" id="txtDesignation">
                            <span class="txtDesignation"></span>
                        </div>
                        <div class="form-group">
                            <label for="message">Select Category</label>
                            <select name="txtCategory" id="txtCategory" class="form-control">
                                <?php 
                                foreach ($memberCategories as $key => $category ) {
                                    echo sprintf('<option value="%s">%s</option>',
                                        $category->id,
                                        $category->cat_name
                                    );
                                }
                                ?>
                            </select>
                            <span class="txtCategory"></span>
                        </div>

                        <div class="btn-wrap form-group">
                            <button type="submit" class="btn lt-blue-bg" >Add Member</button>
                            <img src="<?php echo home_url(); ?>/wp-admin/images/spinner-2x.gif" id="ImgLoder" style="display: none;">
                        </div>
                        <div class="form-group">
                            <div class="alert alert-success" id="MessageDone" style="display:none;"></div>
                            <div class="alert alert-danger" id="MessageFail" style="display:none;"></div>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<?php
	}
}