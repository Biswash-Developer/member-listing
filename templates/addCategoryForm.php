<?php
namespace Inc\templates;

use \Inc\Base\CrudMethod;
use \Inc\Base\AjaxCallBack;

/**
* @package Dwt Travel
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

if ( is_user_logged_in() ) {
	if ( current_user_can( 'manage_options' ) ) { ?>
		<div class="wrap custom-plugin">
		<h1>Add Category</h1>
		
		<div class="row">
			<div class="col-lg-6 test">
                <div class="contact-form-wrap white-bg">
                    <form id="frmAddCategory" method="post">

                        <div class="form-group">
                            <label for="bookingdate">Category Name</label>
                            <input type="text" name="txtCategoryName" class="form-control" id="txtCategoryName">
                            <span class="txtCategoryName"></span>
                        </div>
                        
                        <div class="btn-wrap form-group">
                            <button type="submit" class="btn lt-blue-bg" >Add Category</button>
                            <img src="<?php echo home_url(); ?>/wp-admin/images/spinner-2x.gif" id="ImgLoder" style="display: none;">
                        </div>
                        <div class="form-group">
                            <div class="alert alert-success" id="MessageDone" style="display:none;"></div>
                            <div class="alert alert-danger" id="MessageFail" style="display:none;"></div>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<?php
	}
}