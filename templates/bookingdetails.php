<?php
namespace Inc\templates;

use \Inc\Base\MyDb;
use \Inc\Base\CrudMethod;
/**
* @package Dwt Travel
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

if ( isset( $_GET['id'] ) && $_GET['action'] == 'delete' ) {
	
	CrudMethod::delete_booking_details( $_GET['id'] );

}

?>
<h1>Booking Details.</h1>
<hr/>
<table class="wp-list-table widefat fixed striped bookings">
    <thead>
        <tr>
            <th class="manage-column" width="50px">S.N</th>
            <th class="manage-column">Full Name</th>
            <th class="manage-column">Email</th>
            <th class="manage-column">Request From</th>
            <th class="manage-column">Requested Date</th>
            <th class="manage-column">Status </th>
            <th class="manage-column">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $my_db = new MyDb();
        $capsule = $my_db->capsule();

        $booking_details = $capsule::table('bookingdetails')->get()->toArray();

        if ( count( $booking_details ) > 0 && is_array( $booking_details ) ) {
            $index = 1;
            foreach ( $booking_details as $detail ) {  
                $traveller_detail = unserialize( $detail->traveller_details );
                //persong details 
                $title = ! empty( $traveller_detail[1]['txtNameTitle'] ) ?  $traveller_detail[1]['txtNameTitle'] : '';
                $name  = ! empty( $traveller_detail[1]['txtFullName'] ) ? $traveller_detail[1]['txtFullName']  : '-';
                $email =  ! empty( $traveller_detail[1]['txtEmail'] ) ? $traveller_detail[1]['txtEmail'] : '-';

                ?>
                <tr>
                    <td><?= $index; ?></td>
                    <td><?php echo $title . ' ' . $name; ?></td>
                    <td><?php echo $email; ?></td>
                    <td><?php echo $detail->booking_from; ?></td>
                    <td><?php echo date('d/m/Y', strtotime( $detail->created_at ) ); ?></td>
                    <td>
                        <select>
                            <option>Pending</option>
                            <option>For Review</option>
                            <option>Email Waiting</option>
                            <option>Payment Waiting</option>
                        </select>
                    </td>
                    <td>
                        <?php
                        echo sprintf('<a href="#" id="printInvoice" data-toggle="modal" data-target="#myModal-%s">View</a> || ', $detail->id );
                        echo sprintf('<a href="#">Edit</a> || ' );
                        echo sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $detail->id, __('Delete', 'custom_plugin'));
                        ?>
                    </td>
                </tr>
                <?php 
                $index++;
            } 
        }
            ?>
    </tbody>
</table>

<?php
if ( count( $booking_details ) > 0 && is_array( $booking_details ) ) {
    foreach ( $booking_details as $detail ) : 
        $package_name = $detail->package_name;
        $trip_date = $detail->trip_start_date;
        $trip_end_date = $detail->trip_end_date;
        $trip_cost = $detail->trip_cost;
        $travellers = $detail->no_of_traveller;
        $grand_total = $detail->grand_total;
        $survey     = $detail->survey;
        $payment_method = $detail->payment_method;
        ?>
        <!-- Modal -->
        <div class="modal fade" id="myModal-<?php echo $detail->id ?>" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Booking Details</h4>
                    </div>
                    <div class="modal-body">
                        <Strong>Package Details</Strong>
                        <table class='table borderless'>
                            <tbody class="table">
                                <tr>
                                    <td>Trip Name: </td>
                                    <td><?php echo $package_name; ?></td>
                                </tr>
                                <tr>
                                    <td>Trip Start Date:</td>
                                    <td><?php echo $trip_date; ?></td>
                                </tr>
                                <tr>
                                    <td>Trip End Date:</td>
                                    <td><?php echo $trip_end_date; ?></td>
                                </tr>
                                <tr>
                                    <td>No of Pax:</td>
                                    <td><?php echo $travellers; ?></td>
                                </tr>
                                <tr>
                                    <td>Grand Total:</td>
                                    <td> USD <?php echo $trip_cost; ?></td>
                                </tr>
                                <tr>
                                    <td>Payment Method:</td>
                                    <td><?php echo $payment_method; ?></td>
                                </tr>
                                <tr>
                                    <td>How to know?</td>
                                    <td><?php echo $survey; ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <?php
                        $traveller_detail = unserialize( $detail->traveller_details );

                        if ( ! empty( $traveller_detail ) && count( $traveller_detail ) > 0 ) { 
                            
                            $i = 1;

                            foreach ( $traveller_detail as $person ) {  
                                $name = $person['txtNameTitle'] . ' ' . $person['txtFullName'];
                                ?>
                                <Strong>Traveller Details <?= $i; ?> </Strong>
                                <table class='table borderless'>
                                    <tbody class="table">
                                        <tr>
                                            <td>Full Name: </td>
                                            <td><?= $name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email:</td>
                                            <td><?= $person['txtEmail']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>County:</td>
                                            <td><?= $person['txtCountry']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth: </td>
                                            <td><?= $person['txtBirthDate']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Emergency No: </td>
                                            <td><?= $person['txtEmergencyNo']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No:</td>
                                            <td><?= $person['txtMobileNo']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Passport No: </td>
                                            <td>P<?= $person['txtPasswordNo']; ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Place of issue: </td>
                                            <td><?= $person['txtPlaceOfIssuePassport']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Issue Date: </td>
                                            <td><?= $person['txtPassportIssueDate']; ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Expire Date: </td>
                                            <td><?= $person['txtPassportExpireDate']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Isurance Details:</td>
                                            <td><?= $person['txtInsuranceDetails']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                                $i++;
                            } 
                        } ?>
                    </div>
                    <div class="modal-footer">
                    <a href="#">
                        <button type="button" class="btn btn-secondary hvr-ripple-out">Download PDF</button>
                    </a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
    endforeach;
}