<?php
namespace Inc\templates;

use \Inc\Base\CrudMethod;
use \Inc\Base\AjaxCallBack;
use \Inc\Base\Custompdf;

/**
* @package Dwt Travel
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

if ( is_user_logged_in() ) {
	if ( current_user_can( 'manage_options' ) ) {
        if ( isset( $_GET['id'] ) && ! empty( $_GET['id'] ) ) {
            $memberCategories = CrudMethod::getMemberCategoryLists();
            $member = CrudMethod::eidt_available_date( $_GET['id'] );
        
            $firstName = ! empty( $member->firstName ) ? $member->firstName : '';
            $lastName = ! empty( $member->lastName ) ? $member->lastName : '';
            $email  = ! empty( $member->email ) ? $member->email : '';
            $district  = ! empty( $member->district ) ? $member->district : '';
            $designation =  ! empty( $member->designation ) ? $member->designation : '';
            $oldCategory =  ! empty( $member->category_id ) ? $member->category_id : null;
        }

        $response = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['memberId']) && ! empty($_POST['memberId']) ) {
            $response = Custompdf::generatePdf($_POST['memberId']);
        }

        ?>
		<div class="wrap custom-plugin">
		<h1>Edit Member List</h1>
       
		<div class="row">
			<div class="col-lg-6 test">
                <div class="contact-form-wrap white-bg">
                    <form id="frmEditAvailableDates" method="post">
                        <input type="hidden" name="edite_id" value="<?php echo $member->id; ?>">

                        <div class="form-group">
                            <label for="bookingdate">First Name</label>
                            <input type="text" name="txtFirstName" class="form-control" id="txtFirstName" value="<?php echo $firstName; ?>">
                            <span class="txtFirstName"></span>
                        </div>
                        
                        <div class="form-group">
                            <label for="mobile">Last Name</label>
                            <input type="text" class="form-control" name="txtLastName" id="txtLastName" value="<?php echo $lastName; ?>">
                            <span class="txtLastName"></span>
                        </div>

                        <div class="form-group">
                            <label for="message">Email</label>
                            <input type="text" class="form-control" name="txtEmail" id="txtEmail" value="<?php echo $email; ?>">
                            <span class="txtEmail"></span>
                        </div>
                        <div class="form-group">
                            <label for="message">District</label>
                            <input type="text" class="form-control" name="txtDistrict" id="txtDistrict" value="<?php echo $district; ?>">
                            <span class="txtDistrict"></span>
                        </div>
                        <div class="form-group">
                            <label for="message">Designation</label>
                            <input type="text" class="form-control" name="txtDesignation" id="txtDesignation" value="<?php echo $designation; ?>">
                            <span class="txtDesignation"></span>
                        </div>
                        <div class="form-group">
                            <label for="message">Select Category</label>
                            <select name="txtCategory" id="txtCategory" class="form-control">
                                <?php 
                                foreach ($memberCategories as $key => $category ) {
                                    echo sprintf('<option value="%s" %s>%s</option>',
                                        $category->id,
                                        selected($category->id, $oldCategory ),
                                        $category->cat_name
                                    );
                                }
                                ?>
                            </select>
                            <span class="txtCategory"></span>
                        </div>

                        <div class="btn-wrap form-group">
                            <button type="submit" class="btn lt-blue-bg" style="d-none">Update</button>
                            <img src="<?php echo home_url(); ?>/wp-admin/images/spinner-2x.gif" id="ImgLoder" style="display: none;">
                        </div>
                        <div class="form-group">
                            <div class="alert alert-success" id="MessageDone" style="display:none;">Member Updated</div>
                            <div class="alert alert-danger" id="MessageFail" style="display:none;">Sorry try again.</div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6 test">
                <form action="" method="POST">
                    <input type="hidden" name="memberId" value="<?php echo sanitize_text_field($_GET['id']); ?>">
                    <button type="submit" class="btn btn-success">Send Certificate</button>
                </form><a href=""></a>
                <?php
                if ($response === false ) {
                    echo "sorry try again";
                }

                if ($response === true ) {
                    echo "Mail Sent <br/>";
                    echo sprintf('<a href="%s" class="">Back To List</a>', 
                        admin_url(). '/admin.php?page=dwtEmailLights'
                    );
                }
                ?>
            </div>
		</div>
	</div>
	<?php
	}
}