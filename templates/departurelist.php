<?php
namespace Inc\templates;

use \Inc\Base\MyDb;
use \Inc\Base\CrudMethod;
use \Inc\Base\ListTables;
use \Inc\Base\AjaxCallBack;


/**
* @package Dwt Travel
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ) {
	return false;
}

global $wpdb;
$table = new ListTables();
$table->prepare_items();
$message = '';

if ('delete' === $table->current_action()) {
	$message = '<div class="updated below-h2" id="message"><p>Member Deleted. </p></div>';
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['csvUpload']) && isset($_FILES['csvFile']) && ! empty($_FILES['csvFile']) && $_FILES['csvFile']['error'] === UPLOAD_ERR_OK) {
	$file_extension = pathinfo($_FILES['csvFile']['name'], PATHINFO_EXTENSION);
	if ($file_extension !== 'csv') {
		echo 'Only CSV files are allowed.';
		exit;
	}
	$response = CrudMethod::uploadCsvFile($_FILES['csvFile']);

	if ($response) {
		header("Refresh:0");
	}
}
?>
<div class="wrap">
	<div class="icon32 icon32-posts-post" id="icon-edit"></div>
	<h2><?php _e('Member Lists', 'custom_table_example'); ?>
		<span class="add-new-button">
			<a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=dwt_add_member_form'); ?>">
				<?php _e('Add new', 'custom_tble_example'); ?>
			</a>
		</span>
	</h2>

	<div class="member-lists-actions">
		<form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>?page=dwtEmailLights" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="csvUpload">
			<label for="csvFile"><?php _e('Upload CSV:', 'custom_table_example'); ?></label>
			<input type="file" name="csvFile" id="csvFile">
			<input type="submit" name="uploadCSV" class="button" value="<?php _e('Upload', 'custom_table_example'); ?>">
		</form>

		<form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>?page=dwtEmailLights" method="POST">
			<?php 
				echo $message; 
				$table->display(); 
			?>
		</form>
	</div>
</div>
