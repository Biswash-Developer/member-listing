<?php
namespace Inc\Base;
/**
* @package fixed-departure
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class Deactivate
{
	public static function Deactivate(){
		flush_rewrite_rules();
	}
}