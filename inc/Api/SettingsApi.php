<?php
 namespace Inc\Api;
/**
* @package fixed-departures
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class SettingsApi
{
	public $admin_pages = array();

	public $Admin_SubPages = array();

	public function registers(){
		if( ! empty( $this->admin_pages ) ){
			add_action( 'admin_menu', array($this, 'add_admin_menu' ) );
		}
	}

	public function AddPages( array $pages ){
		$this->admin_pages = $pages;
		return $this;
	}

	public function WithSubPages(string $title = null ){
		if( empty( $this->admin_pages ) ){
			return $this;
		}

		$admin_page = $this->admin_pages[0];
        $SubPages = array(
        	array(
        		'parent_slug' => $admin_page['menu_slug'],
        		'page_title'  => $admin_page['page_title'],
        		'menu_title'  => ($title) ? $title : $admin_page['page_title'],
        		'capability'  => $admin_page['capability'],
        		'menu_slug'	  => $admin_page['menu_slug'],
        		'callback'    => function() { }
        	)
        );

        $this->Admin_SubPages = $SubPages;

        return $this;
	}

	public function addSubPages( array $pages ){
		$this->Admin_SubPages = array_merge($this->Admin_SubPages, $pages);
		return $this;
	}

	public function add_admin_menu(){
		foreach( $this->admin_pages as $page ){
			add_menu_page( $page['page_title'], $page['menu_title'], $page['capability'], $page['menu_slug'], $page['callback'], $page['icon_url'], $page['position'] );
		}

		foreach( $this->Admin_SubPages as $page ){
			add_submenu_page( $page['parent_slug'], $page['page_title'], $page['menu_title'], $page['capability'], $page['menu_slug'], $page['callback'] );
		}

	}
}
