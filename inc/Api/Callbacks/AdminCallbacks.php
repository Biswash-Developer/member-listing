<?php
/**
 * @package: Fixed Departures
 */

namespace Inc\Api\Callbacks;
use \Inc\Base\BaseController;

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class AdminCallbacks extends BaseController {
	
	public function FixedDeparture(){
		return require_once("{$this->plugin_path}/templates/departurelist.php" );
	}

	public function EditDeparture(){
		return require_once("{$this->plugin_path}/templates/edit-departure-form.php" );
	}

	public function AddFixedDeparture(){
		return require_once("{$this->plugin_path}/templates/departures_form_page_handler.php" );
	}

	public function addMemberCategory(){
		return require_once("{$this->plugin_path}/templates/addCategoryForm.php" );
	}

	public function OptionPage() {
		return require_once("{$this->plugin_path}/templates/bookingdetails.php" );
	}
}
