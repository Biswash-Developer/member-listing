<?php
namespace Inc\Base;
use \Inc\Base;

/**
* @package fixed departure
* @version 1.0.0
* @author Dipak Dhakal
* @param nothing
* @return nothing
*/
if( ! defined( 'ABSPATH' ) ){
	return false;
}

class CreateDatabaseTable {
	private $capsule;

	public function __construct( $capsule ) {
		$this->capsule = $capsule;
		$this->create_tables();
	}

	public function create_tables() {

		$capsule = $this->capsule;
		if (! $capsule::schema()->hasTable('member_category')) {
			$capsule::schema()->create('member_category', function ($table) {
				$table->increments('id');
				$table->string('cat_name', 200)->nullable();
				$table->enum('status', ['enable', 'disable'])->default('enable');
				$table->timestamps();
			});
		}
		
		if (! $capsule::schema()->hasTable('memberlists')) {
			$capsule::schema()->create('memberlists', function ($table) {
				$table->increments('id');
				$table->string('name', 200)->nullable();
				$table->string('firstName', 200)->nullable();
				$table->string('lastName')->nullable();
				$table->string('email')->nullable();
				$table->string('district')->nullable();
				$table->string('designation')->nullable();
				$table->integer('category_id')->unsigned()->nullable();
				$table->enum('status', ['unsend', 'sent'])->default('unsend');
				$table->timestamps();
		
				// Add foreign key constraint
				$table->foreign('category_id')->references('id')->on('member_category')->onDelete('set null');
			});
		}
		
	}
}

