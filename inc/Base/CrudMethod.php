<?php 
namespace Inc\Base;

use \Inc\Models\MemberCategory;
use \Inc\Models\Memberlist;

/**
* @package fixed-departure
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class CrudMethod {
    /**
     * Save Fixed Departures Dates
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.royalholidaytravelpackage.com.
    */
	public static function saveavailabledated( $postdata = array() ) {
        if ( ! empty( $postdata['txtFirstName'] ) && ! empty( $postdata['txtEmail'] ) ) {
            Memberlist::updateOrCreate( [
                'firstName'    => $postdata['txtFirstName'],
                'lastName'     => $postdata['txtLastName'],
                'name'         => $postdata['txtFirstName'] . ' '.$postdata['txtLastName'], 
                'email'        => $postdata['txtEmail'],
                'district'     => $postdata['txtDistrict'],
                'designation'  => $postdata['txtDesignation'],
                'category_id'  => $postdata['txtCategory']
            ] );
            return true;
        } else {
            return false;
        }

    }

    /**
     * Save Member Category
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.royalholidaytravelpackage.com.
    */
	public static function saveMemberCategory( $postdata = array() ) {
        if ( ! empty( $postdata['txtCategoryName'] ) ) {
            MemberCategory::updateOrCreate( [
                'cat_name' => $postdata['txtCategoryName'],
                'status'   => 'enable',
            ] );
            return true;
        } else {
            return false;
        }

    }

    /**
     * Save Fixed Departures Dates
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.wildstone.com.
    */
	public static function uploadCsvFile( $file = array() ) {
        
        if ( isset( $file['name'] ) && ! empty( $file['tmp_name'] ) ) {
            
            $csvFile = fopen($file['tmp_name'], 'r');
            if ($csvFile !== false) {
                while ( ($data = fgetcsv($csvFile, 10000, ",") ) !== false) {
                    Memberlist::updateOrCreate( [
                        'firstName'     => $data[0],
                        'lastName'      => $data[1],
                        'name'          => $data[0]. ' '. $data[1],
                        'email'         => $data[2],
                        'district'      => $data[3],
                        'designation'   => $data[4],
                        'category_id'   => $data[5]
                    ] );
                }
                fclose($csvFile);
                
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Delete Fixed Departus Dates
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.royalholidaytravelpackage.com.
     */
    public static function delete_available_date( $id ) {

        if ( empty( $id ) ) return false;
        $dates = Memberlist::find( $id );

        $deleted = $dates->delete();
    }

    /**
     * Edite Fixed Departus Dates  
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.royalholidaytravelpackage.com.
     */
    public static function eidt_available_date( $id ) {
        if ( empty( $id ) ) return false;

        return Memberlist::find( $id );

    }

    /**
     * Save edited Fixed departures dates 
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.royalholidaytravelpackage.com.
     */
    public static function save_edited_available_dates( $id, $postdata = array() ) {
        if ( empty( $id ) ) return false;

        //updating DATAS
        $updated = Memberlist::where( 'id', $id )->update([
            'firstName'    => $postdata['txtFirstName'],
            'lastName'     => $postdata['txtLastName'],
            'name'         => $postdata['txtFirstName'] . ' '.$postdata['txtLastName'], 
            'email'        => $postdata['txtEmail'],
            'district'     => $postdata['txtDistrict'],
            'designation'  => $postdata['txtDesignation'],
            'category_id'  => $postdata['txtCategory']
        ] );

        if ( $updated ) {
            return true;
        } else {
            return false;
        }
                
    }

    /**
     * List Member Category Lists
     * @version 1.0.0
     * @author Diapk Dhakal 
     * @author URI www.dipakdhakal.com.
     */
    public static function getMemberCategoryLists() {
        return MemberCategory::select('id', 'cat_name')->get();
    }

}