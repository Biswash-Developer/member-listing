<?php
namespace Inc\Base;
use \WP_List_Table;
use \Inc\Base\CrudMethod;
use \Inc\Base\Custompdf;
/**
* @package fixed-departures
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

class ListTables  extends WP_List_Table  {
    
    /**
     * [REQUIRED] You must declare constructor and give some basic params     
     * 
    */
    function __construct() {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'memberlist',
            'plural' => 'memberlists',
            'ajax' => false
        ));
    }

    /**
     * [REQUIRED] this is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name) {

        if ($column_name === "category_id") {
            return $this->get_category_name($item['category_id']);
        }

        if ($column_name === "status") {

            if ( $item['status'] === 'unsend' ){
                return sprintf('<a href="#" style="color: red">Email Unsend</a>', );
            }else {
                return sprintf('<a href="#" style="color: green">Email Sent</a>', );
            }
        }

        return $item[$column_name];
    }

    private function get_category_name($category_id) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'member_category';

        $query = $wpdb->prepare("SELECT cat_name FROM $table_name WHERE id = %d", $category_id);
        $category_name = $wpdb->get_var($query);

        return $category_name ? $category_name : '';
    }

    /**
     * [OPTIONAL] this is example, how to render specific column
     *
     * method name must be like this: "column_[column_name]"
     *
     * @param $item - row (key, value array)
     * @return HTML
     */     
    function column_start_date($item){
        return '<em>' . $item['firstName'] . '</em>';
    }
    
    function column_max_pax($item){
        return '<em>' . $item['lastName'] . '</em>';
    }

    /**
     * Display the search box.
     *
     * @param string $text       The search box label.
     * @param string $input_id   The ID attribute for the search input field.
     */
    public function search_box($text, $input_id) {
        ?>
        <p class="search-box">
            <label class="screen-reader-text" for="<?php echo esc_attr($input_id); ?>"><?php echo esc_html($text); ?>:</label>
            <input type="search" id="<?php echo esc_attr($input_id); ?>" name="s" value="<?php _admin_search_query(); ?>" />
            <?php submit_button($text, 'button', '', false); ?>
        </p>
        <?php
    }

    /**
     * [OPTIONAL] this is example, how to render column with actions,
     * when you hover row "Edit | Delete" links showed     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_name($item) {
        $actions = array(
            'edit' => sprintf('<a href="?page=dwt_edit_member_form&id=%s">%s</a>', $item['id'], __('Edit', 'custom_table_example')),
            'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('Delete', 'custom_table_example')),
        );

        return sprintf('%s %s',
            ucfirst(str_replace("_"," ", $item['name'] ) ),
            $this->row_actions($actions)
        );
    }

    /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }
    /**
     * [REQUIRED] This method return columns to display in table
     * you can skip columns that you do not want to show
     * like content, or description
     *
     * @return array
     */
    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'name'  => __('Full Name', 'custom_table_example'),
            'email' => __('Email', 'custom_table_example'),
            'district' => __('District', 'custom_table_example'),
            'category_id' => __('Category', 'custom_table_example'),
            'designation' => __('Designation', 'custom_table_example'),
            'status' => __('Email Status', 'custom_table_example'),
        );
        return $columns;
    }

    /**
     * [OPTIONAL] This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns() {       
        $sortable_columns = array(
            'name' => array('name', true),
            'email' => array('email', false),
            'category_id' => array('category_id', false),
            'status' => array('status', false),
        );
        return $sortable_columns;
    }

    function extra_tablenav($which) {
        if ($which === 'top') {
            $emailStatus = isset($_REQUEST['emailStatus']) ? $_REQUEST['emailStatus'] : '';
            $oldCategory = isset($_REQUEST['memberCategory']) ? $_REQUEST['memberCategory'] : '';
            ?>
            <div class="alignleft actions">
                <select name="memberCategory" id="memberCategory">
                    <option value="">All Categories</option>
                    <?php
                    $memberCategories = CrudMethod::getMemberCategoryLists();
                    foreach ($memberCategories as $key => $category ) {
                        echo sprintf('<option value="%s" %s>%s</option>',
                            $category->id,
                            selected($category->id, $oldCategory ),
                            $category->cat_name
                        );
                    }
                    ?>
                </select>
                <select name="emailStatus" id="emailStatus">
                    <option value="">Email Status</option>
                    <option value="sent" <?php selected($emailStatus, 'sent'); ?>>Email Sent</option>
                    <option value="unsend" <?php selected($emailStatus, 'unsend'); ?>>Email Unsend</option>
                </select>

                <input type="submit" name="filter" id="post-query-submit" class="button" value="Filter" />
            </div>
            <?php
        }
    }
    
    function get_bulk_actions(){
        $actions = array(
            'delete' => 'Delete',
            'emailSend' => 'Send Email',
        );

        return $actions;
    }

    /**
     * [OPTIONAL] This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     */
    function process_bulk_action(){
        global $wpdb;
        $table_name = $wpdb->prefix . 'memberlists'; // do not forget about tables prefix
        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);
            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }

        //send buld Email function
        if ('emailSend' === $this->current_action() ) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            Custompdf::bulkEmailSend($ids);
        }

    }

    /**
     * [REQUIRED] This is the most important method
     *
     * It will get rows from database and prepare them to be showed in table
     */
    function prepare_items() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'memberlists';
        $per_page = 50;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();
    
        $category_filter = isset($_REQUEST['memberCategory']) ? $_REQUEST['memberCategory'] : '';
        $email_status_filter = isset($_REQUEST['emailStatus']) ? $_REQUEST['emailStatus'] : '';
        $search_query = isset($_REQUEST['s']) ? $_REQUEST['s'] : '';
    
        $query = "SELECT * FROM $table_name";
        $count_query = "SELECT COUNT(id) FROM $table_name";
        $where = '';
    
        // Apply filters
        $where .= $this->get_filter_query($category_filter, $email_status_filter);
    
        // Apply search
        if (!empty($search_query)) {
            $where .= $this->get_search_query($search_query);
        }
    
        $total_items = $wpdb->get_var($count_query . $where);
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged'] - 1) * $per_page) : 0;
    
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';
    
        $query .= $where;
        $query .= $wpdb->prepare(" ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged);
    
        $this->items = $wpdb->get_results($query, ARRAY_A);
    
        $this->set_pagination_args(array(
            'total_items' => $total_items,
            'per_page' => $per_page,
            'total_pages' => ceil($total_items / $per_page)
        ));
    }    
    
    function get_filter_query($category_filter, $email_status_filter) {
        global $wpdb;
        $where = ' WHERE 1=1'; // Start with a placeholder condition
    
        if (!empty($category_filter)) {
            $where .= $wpdb->prepare(" AND category_id = %d", $category_filter);
        }
    
        if (!empty($email_status_filter)) {
            $where .= $wpdb->prepare(" AND status = %s", $email_status_filter);
        }
    
        return $where;
    }
    
    
    
    function get_search_query($search_query) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'memberlists';
        
        return $wpdb->prepare(" AND name LIKE %s", '%' . $search_query . '%');
    }
    
    
    function display() {
        $this->search_box('Search', 'custom-search-input');
        parent::display();
    }

}