<?php
namespace Inc\Base;

use \Inc\Base\templates;
use \Inc\Base\CrudMethod;
use \WP_Query;

/**
* @package fixed departure
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/
if( ! defined( 'ABSPATH' ) ){
	return false;
}


class ShowDeparturesDate {
    
    public function __construct() {
        add_action('wp_ajax_ReturnFixedDeparturesDates', array( &$this, 'ReturnFixedDeparturesDates' ) );
        add_action('wp_ajax_nopriv_ReturnFixedDeparturesDates', array( &$this, 'ReturnFixedDeparturesDates' ) );
    }

    public function ReturnFixedDeparturesDates(){

        print_r( $_POST );
        //echo $option;
       die();
    }
}
