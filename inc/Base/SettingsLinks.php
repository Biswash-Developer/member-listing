<?php
namespace Inc\Base;

use \Inc\Base\BaseController;
/**
* @package fixed departure
* @version 1.0.0
* @author Dipak Dhakal
* @param nothing
* @return nothing 
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class SettingsLinks extends BaseController
{

	public function register(){
		add_filter( "plugin_action_links_{$this->plugin_name}", array($this, 'settings_link' ) );
	}

	public function settings_link( $links ){
		$setting_links = '<a href="admin.php?page=departure">setting</a>';
		array_push($links, $setting_links);
		return $links;
	}

}	