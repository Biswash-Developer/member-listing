<?php 
namespace Inc\Base;

use \Inc\Models\Memberlist;
use \Inc\Base\CrudMethod;
use Dompdf\Dompdf;

/**
* @package mass email
* @author WSS
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}


class Custompdf {

    public static function generatePdf( $id ) {
        if ( empty( $id ) ) return false;
        $member = Memberlist::find( $id );
        $email = $member->email;
        $full_name = $member->name;
        $first_name = $member->firstName;
        $image_temp_path = plugin_dir_path(__FILE__).'temp/certificate-of-attendance-cki-meetup-a4.jpg';
        $html = '<html><body><style>@page { margin: 0px; }body { margin: 0px;}</style>
                    <table cellpadding="0" cellspacing="0" border="0">
                       <tr>
                          <td>
                             <img src="data:image/png;base64,'.base64_encode(file_get_contents($image_temp_path)).'" style="max-width:100%; position:relative;">
                             <span style="position:absolute; color:#a28355; font-size:42px; font-family:Helvetica; width:100%; text-align:center; top:430px; left:0px;">'.$full_name.'</span>
                          </td>
                       </tr>
                    </table></body></html>';
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $customPaper = array(0,0,595,842);
        $dompdf->setPaper($customPaper);
        //$dompdf->setPaper('A4', 'portrait');
        // Render the PDF
        $dompdf->render();
        $pdfContent  = $dompdf->output();
        $pluginDirPath = plugin_dir_path(__FILE__);
        $tempFolderPath = $pluginDirPath . 'temp/';
        
        if (!file_exists($tempFolderPath)) {
            mkdir($tempFolderPath);
        }

        $tempFilePath = $tempFolderPath . 'certificate_'.$id.'-'.$member->firstName.'.pdf';
        // Save the PDF to the temporary file
        file_put_contents($tempFilePath, $pdfContent);
        $headers = array(
            'Content-Type: text/html; charset=UTF-8',
        );
        $attachments = array(
            $tempFilePath
        );
        return self::sendMail($email, $full_name, $attachments, $id);
    }

    public static function sendMail( $to, $full_name, $attachments, $id ){
        //$from = 'info@kiwanisaspac.org';
        $from = 'aspacvo@gmail.com';        
        $subject = "Circle K Meet-Up Certificate";
        $msg_details = "Attached is your Certificate of Attendance for the CKI Meet Up. <br><br>Thank you for your support.<br><br>ASPAC VO";
        $headers = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<html><body>';
        $message = '<strong>Dear '.$full_name.',</strong><br><br>'.$msg_details.'';
        $message .= "</body></html>";
        if (wp_mail($to, $subject, $message, $headers, $attachments) ){
            
            //update member email send status.
            Memberlist::where( 'id', $id )->update(['status' => 'sent'] );
            return true;
        }

        return false;
    }

    public static function bulkEmailSend($memberIds = [] ){
        if (count($memberIds) < 0 ) return false;

        foreach ($memberIds as $key => $member ) {
           self::generatePdf($member);
        }

    }

}