<?php
namespace Inc\Base;

use \Inc\Base\BaseController;
/**
* @package fixed departure
* @version 1.0.0
* @author Dipak Dhakal
* @param nothing
* @return nothing
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class Enqueue extends BaseController
{
	public function register(){
		add_action( 'admin_enqueue_scripts', array($this, 'fixed_departure_enqueue' ) );
	}

	public function fixed_departure_enqueue( $hook_suffix ){
		//die( $hook_suffix );
		
		if( $hook_suffix === 'email-light_page_dwt_add_member_form' || 
			$hook_suffix === 'email-light_page_dwt_edit_member_form' || 
			$hook_suffix === 'email-light_page_dwt_add_category') {
			wp_enqueue_style( 
				'bootstrap-css', 
				$this->plugin_url . '/assets/bootstrap/bootstrap.min.css', 
				array(), 
				false, 
				'all' 
			);

			// wp_enqueue_style(
			// 	'bootstarp-datepicker-css', 
			// 	'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css',
			// 	array(), 
			// 	false, 
			// 	'all' 
			// );
			
			wp_enqueue_style( 
				'fixed-departure', 
				$this->plugin_url . '/assets/custom.css', 
				array(), 
				false, 
				'all' 
			);

			wp_enqueue_script('jquery');

			wp_enqueue_script( 
				'bootstrap-js', 
				$this->plugin_url . '/assets/bootstrap/bootstrap.min.js', 
				array('jquery'), 
				false, 
				true 
			);

			// wp_enqueue_script( 
			// 	'bootstrap-datepicker-js', 
			// 	'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js', 
			// 	array(), 
			// 	false, 
			// 	true 
			// );
			
			wp_enqueue_script(
				'custom-js',
				$this->plugin_url . '/assets/js/custom.js',
				array(),
				'',
				true
			);

			wp_localize_script( 'custom-js', 'CustomPlugin', array(
				'admin_ajax' => admin_url( 'admin-ajax.php' ),
			) );
		}
	}
}
