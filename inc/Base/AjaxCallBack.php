<?php
namespace Inc\Base;

use \Inc\Base\templates;
use \Inc\Base\CrudMethod;
use \WP_Query;

/**
* @package fixed departure
* admin templates main page
* @version 1.0.0
* @author Dipak Dhakal
*/
if( ! defined( 'ABSPATH' ) ){
	return false;
}


class AjaxCallBack {
    
    public function __construct() {

        //save all fixed departures dates
        add_action('wp_ajax_saveFixedDepartureDates', array( &$this, 'saveFixedDepartureDates' ) );
        add_action('wp_ajax_nopriv_saveFixedDepartureDates', array( &$this, 'saveFixedDepartureDates' ) );

        //edit fixed departures dates
        add_action('wp_ajax_editFixedDepartureDates', array( &$this, 'editFixedDepartureDates' ) );
        add_action('wp_ajax_nopriv_editFixedDepartureDates', array( &$this, 'editFixedDepartureDates' ) );

        //save all category
        add_action('wp_ajax_saveMemberCategory', array( &$this, 'saveMemberCategory' ) );
        add_action('wp_ajax_nopriv_saveMemberCategory', array( &$this, 'saveMemberCategory' ) );

    }

    public function saveMemberCategory(){
        if ( isset($_POST['data'] ) ) {
            parse_str( $_POST['data'], $output );
            
           $dates = CrudMethod::saveMemberCategory( $output );
           if ( $dates == 1 ) {
                wp_send_json_success('Category successfully added.');
           } else {
                wp_send_json_error('Opps! Try Again' );
           }

        } else {
            return false;
        }
        die();
    }

    public function saveFixedDepartureDates(){
        if ( isset($_POST['data'] ) ) {
            parse_str( $_POST['data'], $output );
            
           $dates = CrudMethod::saveavailabledated( $output );
           if ( $dates == 1 ) {
                wp_send_json_success('Member successfully added.');
           } else {
                wp_send_json_error('Opps! Try Again' );
           }

        } else {
            return false;
        }
        die();
    }

    public function editFixedDepartureDates(){
        if ( isset($_POST['data'] ) ) {
            parse_str( $_POST['data'], $output );
            $dates = CrudMethod::save_edited_available_dates( $output['edite_id'], $output );
            if ( $dates == 1 ) {
                $return = [
                    'message' => 'Updated Success',
                    'return_url' => get_admin_url( get_current_blog_id(), 'admin.php?page=dwtEmailLights' ),
                    'status' => 'Okay'
                ];
                wp_send_json_success( $return );
            } else {
                $return = [
                    'message' => 'Opps! Try Again',
                    'return_url' => get_admin_url( get_current_blog_id(), 'admin.php?page=dwt_edit_member_form='. $output['edite_id'] ),
                    'status' => 'Okay'
                ];
                wp_send_json_error('Opps! Try Again' );
            }

        } else {
            return false;
        }
        die();
    }

}
