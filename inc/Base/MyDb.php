<?php
namespace Inc\Base;

use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;

class MyDb {
	private $capsule;

	public function __construct() {
		global $wpdb;
		$this->capsule = new Capsule;

		$this->capsule->addConnection([
		    'driver'    => 'mysql',
		    'host'      => DB_HOST,
		    'database'  => DB_NAME,
		    'username'  => DB_USER,
		    'password'  => DB_PASSWORD,
		    'charset'   => 'utf8',
		    'collation' => 'utf8_unicode_ci',
		    'prefix'    => $wpdb->prefix,
		]);

		// Set the event dispatcher used by Eloquent models... (optional)
		$this->capsule->setEventDispatcher(new Dispatcher(new Container));

		// Make this Capsule instance available globally via static methods... (optional)
		$this->capsule->setAsGlobal();

		// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
		$this->capsule->bootEloquent();
	}

	//
	public function capsule() {
		return $this->capsule;
	}
}
