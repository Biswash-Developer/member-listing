<?php 
namespace Inc\Base;

/**
* @package fixed departure
* @author Dipak Dhakal
* @version 1.0.0
* @return 
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class BaseController  
{
	public $plugin_path;
	public $plugin_url;
	public $plugin_name;
	public static $table_name;

	public function __construct(){
		global $wpdb;
		$this->plugin_path = plugin_dir_path( dirname( __FILE__, 2 ) );
		$this->plugin_url  = plugin_dir_url( dirname( __FILE__, 2) );
		$this->plugin_name = plugin_basename( dirname( __FILE__, 3) ) .'/fixed-departures.php';
		self::$table_name  = $wpdb->prefix.'fixed_departures';
		
	}
}