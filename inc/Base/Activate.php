<?php
 namespace Inc\Base;
/**
* @package fixed-departures
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class Activate
{
	public static function activate(){
		flush_rewrite_rules();
	}


}
