<?php
namespace Inc\Models;

use \Illuminate\Database\Eloquent\Model;

class Memberlist extends Model {

	public $fillable = [
		'id',
		'name',
		'firstName',
		'lastName',
		'email',
		'district',
		'designation',
		'category_id',
		'status'
	];
}