<?php
namespace Inc\Models;

use \Illuminate\Database\Eloquent\Model;

class MemberCategory extends Model {
	
	protected $table = 'member_category';

	public $fillable = [
		'id',
		'cat_name',
		'status'
	];
}