<?php
namespace Inc;
/**
*@package Dwt Travel
*@author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ) {
	return false;
}

final class Init
{
	public static function get_services(){
		return [
			Base\MyDb::class,
			Base\CrudMethod::class,
			Models\Memberlist::class,
			Models\MemberCategory::class,
			Base\Custompdf::class,
			Base\AjaxCallBack::class,
			Pages\Admin::class,
			Base\Enqueue::class,
			Base\SettingsLinks::class,
		];
	}

	public static function register_services() {
		foreach (self::get_services() as $class) {
			$service = self::instantiate( $class );

			if ( method_exists($service, 'register' ) ) {
				$service->register();
			}
		}
	}

	private static  function instantiate( $class ) {
		$service = new $class();
		return $service;
	}

}
