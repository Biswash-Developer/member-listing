<?php
 namespace Inc\Base;
/**
* @package Dwt Travel
* @author Dipak Dhakal
*/

if( ! defined( 'ABSPATH' ) ){
	return false;
}

class Activate{
	public static function Activate(){
		flush_rewrite_rules();
	}
}