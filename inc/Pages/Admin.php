<?php
namespace Inc\Pages;
use \Inc\Api\SettingsApi;
use \Inc\Base\ListTables;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;
/**
* @package fixed departure
* @version 1.0.0
* @author Dipak Dhakal
* @param nothing
* @return nothing
*/
if( ! defined( 'ABSPATH' ) ){
	return false;
}

class Admin extends BaseController
{
	public $settings;

	public $callbacks;

	public $pages = array();

	public $sub_pages = array();

	public function register(){
		$this->settings = new SettingsApi;
		$this->callbacks = new AdminCallbacks;
		$this->setPages();
		$this->setSubpages();
		$this->settings->AddPages( $this->pages )->WithSubPages('Email Lists')->addSubPages( $this->sub_pages )->registers();
	}

	public function setPages(){
		$this->pages = [
			[
				'page_title' => 'DWT Email Light',
				'menu_title' => 'Email Light',
				'capability' => 'edit_pages',
				'menu_slug'	 => 'dwtEmailLights',
				'callback'	 => array( $this->callbacks, 'FixedDeparture' ),
				'icon_url'   => 'dashicons-email-alt2',
				'position'	 => 110
			],
		];

	}

	public function setSubpages(){
		$this->sub_pages =[

			[
				'parent_slug' => 'dwtEmailLights',
        		'page_title'  => 'Add Member',
        		'menu_title'  => 'Add New',
        		'capability'  => 'manage_options',
        		'menu_slug'	  => 'dwt_add_member_form',
        		'callback'    => array( $this->callbacks, 'AddFixedDeparture' ),
			],

			[
				'parent_slug' => 'dwtEmailLights',
        		'page_title'  => 'Edit Member',
        		'menu_title'  => '',
        		'capability'  => 'manage_options',
        		'menu_slug'	  => 'dwt_edit_member_form',
        		'callback'    => array( $this->callbacks, 'EditDeparture' ),
			],

			[
				'parent_slug' => 'dwtEmailLights',
        		'page_title'  => 'Add Category',
        		'menu_title'  => 'Add Category',
        		'capability'  => 'manage_options',
        		'menu_slug'	  => 'dwt_add_category',
        		'callback'    => array( $this->callbacks, 'addMemberCategory' ),
			]

		];
	}
}
